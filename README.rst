=====
Gluon
=====

A Model-Driven, Extensible Framework for Networking APIs
Gluon provides a framework for specifying, using a model file,
APIs for network forwarding.  It is intended to offer developers
the possibility of quickly prototyping any networking forwarding
system that describes how a packet moves from port to port or
port to the outside world.

* Free software: Apache license
* Wiki: https://wiki.openstack.org/wiki/Gluon
* Source: http://git.openstack.org/cgit/openstack/gluon
* Bugs: https://bugs.launchpad.net/python-gluon
* Documentation: doc/source/index

  * Installation: doc/source/installation
  * User Guide: doc/source/usage
  * Developers: doc/source/devref/index

