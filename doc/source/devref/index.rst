..
      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

      Convention for heading levels in Gluon devref:
      =======  Heading 0 (reserved for the title in a document)
      -------  Heading 1
      ~~~~~~~  Heading 2
      +++++++  Heading 3
      '''''''  Heading 4
      (Avoid deeper levels because they do not render well.)

####################
Gluon Developer Docs
####################

.. toctree::
   :maxdepth: 2

.. include:: high_level_design.inc
.. include:: plugin_wrapper.inc
.. include:: service_binding_model.inc
.. include:: database_migration.inc
.. include:: gluon-auth.inc
.. include:: gluon_api_spec.inc
.. include:: gluon_proton_versioning.inc
.. include:: repo_structure.inc
