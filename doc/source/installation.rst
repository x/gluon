..
      Copyright 2017, AT&T

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

      Convention for heading levels in Gluon documentation:
      =======  Heading 0 (reserved for the title in a document)
      -------  Heading 1
      ~~~~~~~  Heading 2
      +++++++  Heading 3
      '''''''  Heading 4
      (Avoid deeper levels because they do not render well.)

============
Installation
============

There are 3 steps to install Gluon:

* Step 1: Install ``etcd``
* Step 2: Install Gluon Plugin and Proton Server
* Step 3 (Optional): Set up Mechanism Driver for Open Contrail

In addition, OPNFV Danube Release will provide a scenario in which Step 1 and
Step 2 can be done automatically to install Gluon. However, Step 3 (optional)
still needs to be manually set up.

Contents:

.. toctree::
   :maxdepth: 2

   installation/install_etcd.rst
   installation/install_gluon.rst
   installation/install_contrail.rst

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
